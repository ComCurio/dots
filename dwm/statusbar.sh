print_date(){
	date +%a
	date +%d-%m-%y
	echo "|"
	date +%H:%M
}

print_volume(){
	echo "|"
	echo "VOL"
	if [ $(amixer sget Master | grep "Front Left:" | awk '{print $5}' | sed 's/[^0-9]//g') -eq  0 ]
	then
		echo "M"
	else
		echo "$(amixer sget Master | grep "Front Left:" | awk '{print $5}' | sed 's/[^0-9]//g')"
	fi
	echo "|"
}

statusbar(){
	(
	echo "["
	print_volume
	print_date
	echo "]"
	)| tr '\n' '  '| sed 's/\[ | /\[/g; s/ \] /]/g' 
}
	


while true
do
	xsetroot -name "$(statusbar)"
	sleep 10
done
