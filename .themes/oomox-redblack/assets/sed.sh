#!/bin/sh
sed -i \
         -e 's/#070000/rgb(0%,0%,0%)/g' \
         -e 's/#ffffff/rgb(100%,100%,100%)/g' \
    -e 's/#080000/rgb(50%,0%,0%)/g' \
     -e 's/#d20001/rgb(0%,50%,0%)/g' \
     -e 's/#100000/rgb(50%,0%,50%)/g' \
     -e 's/#ffffff/rgb(0%,0%,50%)/g' \
	$@
